# Guida External Script Nextcloud per cat automatico file MD

### Descrizione

Lo script bash, che viene avviato dal cron di Nextcloud ad ogni creazione/aggiornamento di un file .md, genera due file *index.md* e *index.html* contenenti il concatenamento, ordinato per data di ultima modifica, di tutti i file md contenuti in una folder dell'utente, fino ad un massimo di 2000 righe. Per ogni file viene indicato sia il suo collegamento assoluto di Nextcloud, sia il collegamento relativo che permette la navigazione offline attraverso l'*index.html*.

---

### Requisiti

* App Nextcloud "***Workflow external scripts***";

* Script bash "***autoCat.sh***";

* Pandoc.

---

### Installazione

1. Modificare le variabili dello script autoCat.sh

   * **url**: url di Nextcloud;

   * **wwwPath**: percorso della cartella "*html*" di Nextcloud;

   * **dataPath**: percorso della cartella "*data*" di Nextcloud;

   * **logFile**: percorso per la creazione del file di log;
   
   * **maxFiles**: numero max di file MD da indicizzare;

2. Copiare lo script all'interno di una <FOLDER> della webroot di Nextcloud (es. "/var/www/html/external_scripts/autoCat/autoCat.sh";

3. Impostare il proprietario dello script su **www-data** e aggiungere i permessi di esecuzione:

   ```
   chown www-data:www-data /<path>/autoCat.sh
   chmod 774 /<path>/autocat.sh
   ```

4. Aggiungere la **Rule** di esecuzione dello script nell'app **external script**:

   * Amministrazione -> Settings -> External Script

   * Add **Rule Group**:

     * Command to execute:

       ```
       /<path>/autoCat_v2.sh %e %i %a %o %n %x
       ```

     * Add Rule: Mime Type  is text/markdown

   * Save.

5. Impostare il Background Jobs (cron) di Nextcloud da Amministrazione -> Settings -> Basic Settings a proprio piacimento;

6. Installare Pandoc (es. Debian: "apt install pandoc").

---

**ATTENZIONE**: l'esecuzione dello script viene effettuata attraverso un job di Nextcloud. La sua esecuzione non è quindi instantanea, ma dipende dalle impostazioni dei Background Jobs impostate precedentemente.