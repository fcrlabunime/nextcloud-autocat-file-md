#!/bin/bash

# bash /var/www/html/external_scripts/autoCat/autoCat.sh %e %i %a %o %n %x

# ----- EDIT THIS VARIABLES ------------------------------------------
url="https://fcrlab.unime.it/nextcloud";                    # url nextcloud
wwwPath="/var/www/html";                                    # directory di installazione di Nextcloud
dataPath="/var/www/html/data";	                            # directory data di Nextcloud       
logFile="/var/log/"$(basename $0)".log";                    # file di log
generatedFileName="index";
# --------------------------------------------------------------------

ErrExit() {
	echo "Error:" >> $logFile
	echo $1 >> $logFile
	exit $2
}

[[ "$USER" == "www-data" || "$LOGNAME" == "www-data" ]] || ErrExit "Run as www-data user! Current: $USER" 2

event=$1; id=$2; actor=$3; owner=$4; filePath=${5:1}; oldPath=${6:1};
actor=${filePath%%/*}
webDomain="${url}/index.php/apps/files/";

dt=$(date '+%d/%m/%Y %H:%M:%S');

echo "[$dt] [AUTOCAT MD] Action: $1" >> $logFile;
echo "[$dt] [AUTOCAT MD] Actor: $actor" >> $logFile;
echo "[$dt] [AUTOCAT MD] Owner: $owner" >> $logFile;
echo "[$dt] [AUTOCAT MD] File: $filePath" >> $logFile;
echo "[$dt] [AUTOCAT MD] OldPath: $oldPath" >> $logFile;

# If the file was renamed
if [ "$oldPath" = "" ]
then
   path="${filePath// /%20}";
else
   path="${oldPath// /%20}";
fi

# Obtain path and separator
dirPath="$dataPath/$owner/files";
dirFile=${filePath#*files/};
dirFile="$(dirname ${dirFile// /%20})"
startSeparator="[](START:${filePath// /%20})"
file=$(cat "${dirPath}/${filePath#*files/}");

endSeparator='[](END)'
fileHeader="---\n\n> [$event] $filePath\n>\n> Edited by $actor - $dt"
onlineLink="[ONLINE LINK DIRECTORY](${webDomain}?dir=/$dirFile&fileid=$id)"

i=0;
IFS='/' read -r -a array <<< $filePath
for newDir in "${array[@]: 2:${#array[@]}-4}" 
do
    dirPath="${dirPath}/${newDir}";

    chmod 644 "${dirPath}/${generatedFileName}.md"
    chmod 644 "${dirPath}/${generatedFileName}.html"

    updatedFile=$(sed "/^\[\](START:${path//\//\\/})/,/^\[\](END)/d;" "$dirPath/${generatedFileName}.md");
    offlineLink=${filePath#*${dirPath#*files/}/};
    offlineLink="[OFFLINE LINK FILE](${offlineLink// /%20})";

    echo -e "$startSeparator\n\n$fileHeader\n\n$file\n\n$onlineLink\n\n$offlineLink\n\n$endSeparator\n\n${updatedFile}" > "$dirPath/${generatedFileName}.md";
    newReadme=$(cat -s "${dirPath}/${generatedFileName}.md");
    echo -e "$newReadme" > "${dirPath}/${generatedFileName}.md";
    pandoc -f markdown -t html "${dirPath}/${generatedFileName}.md"  > "${dirPath}/${generatedFileName}.html";

    chmod 444 "${dirPath}/${generatedFileName}.md"
    chmod 444 "${dirPath}/${generatedFileName}.html"
done

# Scan new Nextcloud files
echo -e "[$dt] [AUTOCAT MD] Scanning new files in $dataPath/$owner/files\n" >> $logFile;
cd "$wwwPath";
php occ files:scan --path="$owner/files/";
